#!/usr/bin/env bash

mvn clean package

echo 'Copy files...'

scp -i ~/.ssh/id_ed25519 \
    target/movieter-0.0.1-SNAPSHOT.jar \
    living@212.24.101.31:/home/living/

echo 'Restart server...'

ssh -i ~/.ssh/id_ed25519 living@212.24.101.31 << EOF

pgrep java | xargs kill -9
nohup java -jar movieter-0.0.1-SNAPSHOT.jar > log.txt &

EOF

echo 'Bye'