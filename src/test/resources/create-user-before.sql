delete from user_role;
delete from usr;

insert into usr(id, active, password, username) values
(1, true, '$2a$08$fiFPCdXa9L.Olo8ECt5CQOpq4UPR4u0cQpAgJ.RQbUmYn8fRUd8C6', 'admin'),
(2, true, '$2a$08$fiFPCdXa9L.Olo8ECt5CQOpq4UPR4u0cQpAgJ.RQbUmYn8fRUd8C6', 'mike');

insert into user_role(user_id, roles) values
(1, 'USER'), (1, 'ADMIN'),
(2, 'USER');
